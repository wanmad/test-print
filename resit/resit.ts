import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-resit',
  templateUrl: 'resit.html',
})
export class ResitPage {
  kutipans: any = [];
  noacc: string;
  nobil: string;
  noresit: string;
  nama: string;
  tarikh: string;
  masa: string;
  amaun: string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private databaseprovider: DatabaseProvider,private btSerial:BluetoothSerial,private alertCtrl:AlertController) {
    var idPrinter = localStorage.getItem('idPrinter');
    this.getCurrentData(navParams.get("rowid"));
   // this.PrintLogo(idPrinter);
    this.connectBT(idPrinter);

   
    if(idPrinter==null||idPrinter==""||idPrinter==undefined)
    {
      let mno=this.alertCtrl.create({
        title:"Print error! "+idPrinter,
        buttons:['Dismiss']
      });
      mno.present();
    }
    else
    {
      let mno=this.alertCtrl.create({
        title:"Print OK! "+idPrinter,
        buttons:['Dismiss']
      });
      mno.present();
      this.PrintResit(idPrinter);
    }   
  }

  getCurrentData(rowid) {
    this.databaseprovider.getAllKutipanByID(rowid).then(data => {
      this.kutipans = data;
      this.noacc  = this.kutipans[0].noacc;
      this.nobil = this.kutipans[0].nobil;
      this.noresit = this.kutipans[0].noresit; 
      this.nama = this.kutipans[0].nama;
      this.tarikh = this.kutipans[0].tarikh;
      this.masa = this.kutipans[0].masa;
      this.amaun = this.kutipans[0].amaun;
      this.setPrint();
     // localStorage.setItem('printData',this.noacc+"          "+this.noresit+"\n"+this.nama+"\n"+this.tarikh+"            "+this.masa+"\nSewa Bangunan         "+this.toCurrency(this.amaun)+"\n\n\n\n");
    })
    
  }

  setPrint(){
    let data = "     Majlis Daerah Setiu\n           Wisma MDS,\n   22100, Bandar Permaisuri,\n        Setiu, Terengganu\n\n";
        data += "No. Resit : "+this.noresit+"\n";
        data += "Tarikh    : "+this.tarikh+"  "+this.masa+"\n";
        data += "Bayaran   : Tunai\n";
        data += "Operator  : Azman\n\n";
        data += "Perkara                   Jumlah\n";
        data += "--------------------------------\n";
        data += this.noacc+"\n";
        data += this.nama+"\n";
        data += "Sewa Bangunan           "+this.toCurrency(this.amaun)+"\n\n";
        data += "-------------------------------\n";
        data += "Jumlah                  "+this.toCurrency(this.amaun)+"\n\n\n";
        data += "--------------------------------\n";
        data += "Terima Kasih Kerana Menggunakan\n        Perkhidmatan Kami\n";
        data += "  http://mds.terengganu.gov.my\n\n\n\n";
        localStorage.setItem('printData',data);
  }
  connectBT(address){
    return this.btSerial.connect(address);
  }

  toCurrency(amount) {
    return "RM" + amount.toFixed(2);
  };
  
  
  PrintResit(address)
  {
    let printData= localStorage.getItem('printData');  
    //let printData="S-0000005-05 \nARIFFIN BIN  AWANG LONG \n 23/04/2018   9:26 \n Sewa Bangunan      RM200.00";
    
    let xyz=this.connectBT(address).subscribe(data=>{
      this.btSerial.write(printData).then(dataz=>{
        console.log("WRITE SUCCESS",dataz);

        let mno=this.alertCtrl.create({
          title:"Print SUCCESS! ",
          buttons:['Dismiss']
        });
        mno.present();

        xyz.unsubscribe();
        
      },errx=>{
        console.log("WRITE FAILED",errx);
        let mno=this.alertCtrl.create({
          title:"ERROR "+errx,
          buttons:['Dismiss']
        });
        mno.present();
      });
      },err=>{
        console.log("CONNECTION ERROR",err);
        let mno=this.alertCtrl.create({
          title:"ERROR "+err,
          buttons:['Dismiss']
        });
        mno.present();
      });

  }

  tutup(){
    this.navCtrl.setRoot('KutipanPage');
    localStorage.removeItem('printData');
  }

}
